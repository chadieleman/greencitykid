import { store } from '../store'
import actionsType from './actionsType'

export const testAction = () => {
    store.dispatch({ type: actionsType.GET_TEST, payload: true })
}