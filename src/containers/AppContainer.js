import React from 'react'
import { connect } from 'react-redux'

import { withStyles } from '@material-ui/core/styles'
import { Button } from '@material-ui/core'
import { testAction } from '../actions'

const styles = () => ({})

class AppContainer extends React.Component {
    // constructor(props) {
    //   super(props)
    //   this.state = {

    //   }
    // }
  
    render() {
      const { test } = this.props
      return <div>
        <Button
          variant="contained"
          color="primary"
          onClick={() => testAction()}
        >
          Primary
        </Button>
      
      </div>
    }
  }

  const mapStateToProps = (state) => {
      console.log('state : ', state)
    return ({
        test: state.test
    })
  }

export default withStyles(styles)(connect(mapStateToProps)(AppContainer))
